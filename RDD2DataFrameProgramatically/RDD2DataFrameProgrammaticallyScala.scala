package com.etc

import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SQLContext}
import org.apache.spark.{SparkConf, SparkContext}


object RDD2DataFrameProgrammaticallyScala extends App {

  val conf = new SparkConf()
    .setAppName("RDD2DataFrameProgrammaticallyScala")
    .setMaster("local")
  val sc = new SparkContext(conf)
  val sqlContext = new SQLContext(sc)

  // 第一步，构造出元素为Row的普通RDD
  val studentRDD = sc.textFile("D:\\Documents\\Tencent Files\\1433214538\\FileRecv\\第一阶段代码\\第75讲-Spark SQL：使用编程方式将RDD转换为DataFrame\\文档\\students.txt", 1)
    .map { line => Row(line.split(",")(0).toInt, line.split(",")(1).toString, line.split(",")(2).toInt) }

  // 第二步，编程方式动态构造元数据
  val structType = StructType(Array(
    StructField("id", IntegerType, true),
    StructField("name", StringType, true),
    StructField("age", IntegerType, true)))

  // 第三步，进行RDD到DataFrame的转换
  val DataFrame = sqlContext.createDataFrame(studentRDD,structType)

  DataFrame.registerTempTable("students")

  val teenagerDF = sqlContext.sql("select * from students where age<= 18")

  teenagerDF.rdd.collect().foreach(println(_))

}
