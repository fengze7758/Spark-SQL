package com.etc;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.hive.HiveContext;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.Tuple2;
import java.util.*;

/**
 * @author: fengze
 * @description:
 */
public class DailyTop3KeyWord {
    @SuppressWarnings("deprecation")
    public static void main(String[] args) {
        SparkConf conf = new SparkConf()
                .setAppName("DailyTop3KeyWord").setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);
        HiveContext hiveContext = new HiveContext(sc.sc());

        // 伪造出一份数据，查询条件
        // 备注：实际上，在实际的企业项目开发中，很可能，这个查询条件，是通过J2EE平台插入到某个MySQL表中的
        // 然后，这里呢，实际上，通常是会用Spring框架和ORM框架（MyBatis）的，去提取MySQL表中的查询条件
        //需求：
        HashMap<String, List<String>> queryParamMap = new HashMap<String, List<String>>();
        queryParamMap.put("city", Arrays.asList("太原"));
        queryParamMap.put("platform", Arrays.asList("Android"));
        queryParamMap.put("version", Arrays.asList("1.0", "1.2", "1.5", "2.0"));

        // 根据我们实现思路中的分析，这里最合适的方式，是将该查询参数Map封装为一个Broadcast广播变量
        // 这样可以进行优化，每个Worker节点，就拷贝一份数据即可
        final Broadcast<HashMap<String, List<String>>> broadcast =
                sc.broadcast(queryParamMap);

        // 针对HDFS文件中的日志，获取输入RDD
        JavaRDD<String> rawRDD = sc.textFile("hdfs://192.168.88.200:9000/DailyTop3KeyWord.txt");

        // 使用查询参数Map广播变量，进行筛选
        JavaRDD<String> filter = rawRDD.filter(new Function<String, Boolean>() {
            @Override
            public Boolean call(String s) throws Exception {
                //切割原始日志，获取城市，平台和版本
                String[] line = s.split(",");
                String city = line[3];
                String platform = line[4];
                String version = line[5];

                // 与查询条件进行比对，任何一个条件，只要该条件设置了，且日志中的数据没有满足条件
                // 则直接返回false，过滤该日志
                // 否则，如果所有设置的条件，都有日志中的数据，则返回true，保留日志
                HashMap<String, List<String>> value = broadcast.value();

                List<String> cities = value.get("city");
                if (cities.size() > 0 && !cities.contains(city)) {
                    return false;
                }
                List<String> platforms = value.get("platform");
                if (platforms.size() > 0 && !platforms.contains(platform)) {
                    return false;
                }
                List<String> versions = value.get("version");
                if (versions.size() > 0 && !versions.contains(version)) {
                    return false;
                }
                return true;
            }
        });

        // 过滤出来的原始日志，映射为(日期_搜索词, 用户)的格式
        JavaPairRDD<String, String> dateUserRDD = filter.mapToPair(new PairFunction<String, String, String>() {
            @Override
            public Tuple2<String, String> call(String s) throws Exception {
                String[] split = s.split(",");
                String date = split[0];
                String user = split[1];
                String keyWord = split[2];

                return new Tuple2<String, String>(date + "_" + user, keyWord);

            }
        });

        // 进行分组，获取每天每个搜索词，有哪些用户搜索了（没有去重）
        JavaPairRDD<String, Iterable<String>> dateKeyWordUserRDD = dateUserRDD.groupByKey();
        // 对每天每个搜索词的搜索用户，执行去重操作，获得其uv
        JavaPairRDD<String, Long> dateKeyWordUv = dateKeyWordUserRDD.mapToPair(new PairFunction<Tuple2<String, Iterable<String>>, String, Long>() {
            @Override
            public Tuple2<String, Long> call(Tuple2<String, Iterable<String>> tp) throws Exception {
                String dateKeyWord = tp._1;
                Iterator<String> users = tp._2.iterator();

                // 对用户进行去重，并统计去重后的数量
                List<String> distinctUsers = new ArrayList<String>();
                while (users.hasNext()) {
                    String user = users.next();
                    if (!distinctUsers.contains(user)) {
                        distinctUsers.add(user);
                    }
                }
                // 获取uv
                long uv = distinctUsers.size();
                return new Tuple2<String, Long>(dateKeyWord, uv);
            }
        });

        // 将每天每个搜索词的uv数据，转换成DataFrame
        JavaRDD<Row> rowJavaRDD = dateKeyWordUv.map(new Function<Tuple2<String, Long>, Row>() {
            @Override
            public Row call(Tuple2<String, Long> tuple2) throws Exception {
                String date = tuple2._1.split("_")[0];
                String keyWord = tuple2._1.split("_")[1];
                Long uv = tuple2._2;

                return RowFactory.create(date, keyWord, uv);
            }
        });
        List<StructField> structFields = Arrays.asList(
                DataTypes.createStructField("date", DataTypes.StringType, true),
                DataTypes.createStructField("keyWord", DataTypes.StringType, true),
                DataTypes.createStructField("uv", DataTypes.LongType, true)
        );

        StructType structType = DataTypes.createStructType(structFields);

        DataFrame dataFrame = hiveContext.createDataFrame(rowJavaRDD, structType);

        // 使用Spark SQL的开窗函数，统计每天搜索uv排名前3的热点搜索词
        dataFrame.registerTempTable("daily_keyWord_uv");

        DataFrame sql = hiveContext.sql(""
                + "SELECT date,keyword,uv "
                + "FROM ("
                + "SELECT "
                + "date,"
                + "keyword,"
                + "uv,"
                + "row_number() OVER (PARTITION BY date ORDER BY uv DESC) rank "
                + "FROM daily_keyword_uv"
                + ") tmp "
                + "WHERE rank<=3");

        JavaPairRDD<String, String> top3DateKeyWordUvRDD = sql.javaRDD().mapToPair(new PairFunction<Row, String, String>() {
            @Override
            public Tuple2<String, String> call(Row row) throws Exception {
                String date = row.getString(0);
                String keyWord = row.getString(1);
                Long uv = Long.valueOf(String.valueOf(row.get(2)));

                return new Tuple2<String, String>(date, keyWord + "_" + uv);
            }
        });

        JavaPairRDD<String, Iterable<String>> top3DateKeyWordsRDD = top3DateKeyWordUvRDD.groupByKey();
        JavaPairRDD<Long, String> uvDateKeyWordsRDD = top3DateKeyWordsRDD.mapToPair(new PairFunction<Tuple2<String, Iterable<String>>, Long, String>() {
            @Override
            public Tuple2<Long, String> call(Tuple2<String, Iterable<String>> tp) throws Exception {
                String date = tp._1;

                Long totalUv = 0L;
                String dateKeyWords = date;

                Iterator<String> keyWordUvIterator = tp._2.iterator();
                while (keyWordUvIterator.hasNext()) {
                    String keyWordUv = keyWordUvIterator.next();

                    Long uv = Long.valueOf(keyWordUv.split("_")[1]);
                    totalUv += uv;

                    dateKeyWords += "," + keyWordUv;
                }
                return new Tuple2<Long, String>(totalUv, dateKeyWords);
            }
        });

        // 按照每天的总搜索uv进行倒序排序
        JavaPairRDD<Long, String> sortedUvDateKeyWordRDD = uvDateKeyWordsRDD.sortByKey(false);

        // 再次进行映射，将排序后的数据，映射回原始的格式，Iterable<Row>
        JavaRDD<Row> sortedRowRDD = sortedUvDateKeyWordRDD.flatMap(new FlatMapFunction<Tuple2<Long, String>, Row>() {
            @Override
            public Iterable<Row> call(Tuple2<Long, String> tp) throws Exception {
                String dateKeyWord = tp._2;
                String[] dateKeyWordSplited = dateKeyWord.split(",");
                String date = dateKeyWordSplited[0];

                List<Row> rows = new ArrayList<Row>();
                rows.add(RowFactory.create(date, dateKeyWordSplited[1].split("_")[0],
                        Long.valueOf(dateKeyWordSplited[1].split("_")[1])));

                rows.add(RowFactory.create(date, dateKeyWordSplited[2].split("_")[0],
                        Long.valueOf(dateKeyWordSplited[2].split("_")[1])));

                rows.add(RowFactory.create(date, dateKeyWordSplited[1].split("_")[0],
                        Long.valueOf(dateKeyWordSplited[3].split("_")[1])));
                return rows;
            }

        });

        // 将最终的数据，转换为DataFrame，并保存到Hive表中
        DataFrame finalDF = hiveContext.createDataFrame(sortedRowRDD, structType);

//        finalDF.saveAsTable("daily_top3_keyword_uv");

        List<Row> collect = finalDF.javaRDD().collect();
        for (Row row : collect) {
            System.out.println(row);
        }
        sc.close();

    }
}
