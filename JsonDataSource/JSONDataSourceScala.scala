package com.etc

import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * author: fengze
  * description:
  */
object JSONDataSourceScala {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
      .setAppName("JSONDataSourceScala")
      .setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)

    // 创建学生成绩DataFrame
    val json = sqlContext.read.json("F:\\students.json")

    // 查询出分数大于80分的学生成绩信息，以及学生姓名
    json.registerTempTable("student_core")
    val studentsNameDF = sqlContext.sql("select name,score from student_core where score >= 80")

    //将DataFrame转换为rdd，执行transformation操作
    val studentNameRDD = studentsNameDF.rdd.map(row => row.get(0)).collect()

    // 创建学生基本信息DataFrame
    val studentInfoJSONs = Array(
      "{\"name\":\"Leo\", \"age\":18}",
      "{\"name\":\"Marry\", \"age\":17}",
      "{\"name\":\"Jack\", \"age\":19}")

    val studentInfoJSONsRDD = sc.parallelize(studentInfoJSONs, 3)
    val studentInfosDF = sqlContext.read.json(studentInfoJSONsRDD)

    // 查询分数大于80分的学生的基本信息
    studentInfosDF.registerTempTable("student_info")
    var sql = "select name,age from student_info where name in ("
    for (i <- 0 until studentNameRDD.length) {
      sql += "'" + studentNameRDD(i) + "'"
      if (i < studentNameRDD.length - 1) {
        sql += ","
      }
    }
    sql += ")"
    val goodStudentInfoDF = sqlContext.sql(sql)

    // 将分数大于80分的学生的成绩信息与基本信息进行join
    val goodStudentsRDD1 = studentsNameDF.rdd.map { row => (row.getAs[String]("name"), row.getAs[Long]("score")) }
    val goodStudentsRDD2 = studentInfosDF.rdd.map { row => (row.getAs[String]("name"), row.getAs[Long]("age")) }
    val dsf = goodStudentsRDD1.join(goodStudentsRDD2)

    // 将rdd转换为DataFrame
    val goodStudentRowsRDD = dsf.map(info => Row(info._1, info._2._1.toInt, info._2._2.toInt))

    val structType = StructType(Array(
      StructField("name", StringType, true),
      StructField("score", IntegerType, true),
      StructField("age", IntegerType, true)))

    val goodStudentsDF = sqlContext.createDataFrame(goodStudentRowsRDD, structType)

    // 将DataFrame中的数据保存到json中
    goodStudentInfoDF.write.format("json").save("F:\\goodStudent")
  }
}
