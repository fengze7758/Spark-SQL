package com.etc

import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.{SparkConf, SparkContext}

/**
  * author: fengze
  * description:
  */
object RowNumberWindowsFunctionScala {

  def main(args: Array[String]): Unit = {

    val conf = new SparkConf()
      .setAppName("RowNumberWindowsFunctionScala")
      .setMaster("local")
    val sc = new SparkContext(conf)
    val hiveContext = new HiveContext(sc)

    hiveContext.sql("DROP TABLE IF EXISTS sales")
    hiveContext.sql("CREATE TABLE IF NOT EXISTS sales(product STRING,category STRING,revenue BIGINT)")
    hiveContext.sql("LOAD DATA LOCAL INPATH 'D:\\Documents\\Tencent Files\\1433214538\\FileRecv\\第一阶段代码\\第84讲-Spark SQL：开窗函数以及top3销售额统计案例实战\\文档\\sales.txt' INTO TABLE sales")

    val top3SalesDF = hiveContext.sql("SELECT product,category,revenue FROM " + "(SELECT product,category,revenue," + " row_number() " + "OVER (PARTITION BY category ORDER BY revenue DESC) rank FROM sales ) " + "tmp_sales WHERE rank <= 3")

    hiveContext.sql("DROP TABLE IF EXISTS top3_sales")
    top3SalesDF.rdd.foreach(println(_))
  }
}
