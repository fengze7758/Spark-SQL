package com.etc

import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.{SparkConf, SparkContext}

/**
  * author: fengze
  * description:
  * Hive数据源
  */
object HiveDataSourceScala {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
      .setAppName("HiveDataSourceScala")
    val sc = new SparkContext(conf)
    val hiveContext = new HiveContext(sc)

    // 第一个功能，使用HiveContext的sql()方法，可以执行Hive中能够执行的HiveQL语句

    // 将学生基本信息数据导入student_infos表
    hiveContext.sql("DROP TABLE IF EXISTS student_infos")
    hiveContext.sql("CREATE TABLE IF NOT EXISTS student_infos(name STRING,age INT)")
    hiveContext.sql("LOAD DATA "
      + "LOCAL INPATH 'D:\\Documents\\Tencent Files\\1433214538\\FileRecv\\第一阶段代码\\第81讲-Spark SQL：Hive数据源复杂综合案例实战\\文档\\student_infos.txt'"
      + "INTO TABLE student_infos")

    // 用同样的方式给student_scores导入数
    hiveContext.sql("DROP TABLE IF EXISTS student_scores")
    hiveContext.sql("CREATE TABLE IF NOT EXISTS student_scores(name STRING,score INT)")
    hiveContext.sql("LOAD DATA "
      + "LOCAL INPATH 'D:\\Documents\\Tencent Files\\1433214538\\FileRecv\\第一阶段代码\\第81讲-Spark SQL：Hive数据源复杂综合案例实战\\文档\\student_scores.txt'"
      + "INTO TABLE student_scores")

    // 第二个功能，执行sql还可以返回DataFrame，用于查询

    //    val goodStudentsDF = hiveContext.sql("SELECT si.name, si.age, ss.score "
    //      + "FROM student_infos si "
    //      + "JOIN student_scores ss ON si.name=ss.name "
    //      + "WHERE ss.score>=80");

    val goodStudentsDF = hiveContext.sql("select student_infos.name,student_infos.age,student_scores.score " +
      "from student_infos   join student_scores  on student_infos.name=student_scores.name where student_scores.score>=80")

    // 第三个功能，可以将DataFrame中的数据，理论上来说，DataFrame对应的RDD的元素，是Row即可

    hiveContext.sql("DROP TABLE IF EXISTS good_student_infos")
    goodStudentsDF.saveAsTable("good_student_infos")

    // 第四个功能，可以用table()方法，针对hive表，直接创建DataFrame
    val rows = hiveContext.table("good_student_infos").collect()
    rows.foreach(row => println(row))

    //遍历另一种：
    //for(goodStudentRow <-rows){

    //   println(goodStudentRow)

    //}
  }
}
