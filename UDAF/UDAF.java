package com.etc;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import java.util.Arrays;
import java.util.List;


/**
 * @author: fengze
 * @description: SparkSQL编写UDAF自定义函数（JAVA）
 */
public class UDAF {
    public static void main(String[] args) {
        SparkConf conf = new SparkConf()
                .setAppName("UDAF")
                .setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(sc.sc());

        List<String> list = Arrays.asList("Leo", "Marry", "Jack", "Tom", "Tom", "Tom", "Leo");

        JavaRDD<String> nameRDD = sc.parallelize(list, 5);

        JavaRDD<Row> map = nameRDD.map(new Function<String, Row>() {
            @Override
            public Row call(String s) throws Exception {
                return RowFactory.create(s);
            }
        });

        List<StructField> structFields = Arrays.asList
                (DataTypes.createStructField("name", DataTypes.StringType, true));
        StructType structType = DataTypes.createStructType(structFields);

        DataFrame nameFrame = sqlContext.createDataFrame(map, structType);

        nameFrame.registerTempTable("names");

        sqlContext.udf().register("strCount", new StringCount());

        DataFrame sql = sqlContext.sql("select name,strCount(name) from names group by name");

        List<Row> name = sql.javaRDD().collect();

        for (Row row : name) {
            System.out.println(row);
        }


    }
}
