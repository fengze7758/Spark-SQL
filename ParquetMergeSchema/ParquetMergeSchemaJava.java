package com.etc;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SaveMode;

import java.sql.Array;

/**
 * @author: fengze
 * @description:
 * Parquet数据源之合并元数据
 */
public class ParquetMergeSchemaJava {
    public static void main(String[] args) {
        SparkConf conf = new SparkConf()
                .setAppName("ParquetMergeSchemaJava")
                .setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(sc);

        //创建第一个studentInfo1基本信息，写入一个parquet文件中
        DataFrame studentInfo1 = sqlContext.read().format("json").load("F:\\studentInfo1.json");
        studentInfo1.write().mode(SaveMode.Append).save("F:\\studentInfo.parquet");

        //创建第两个studentInfo1基本信息，写入一个parquet文件中
        DataFrame studentInfo2 = sqlContext.read().format("json").load("F:\\studentInfo2.json");
        studentInfo2.write().mode(SaveMode.Append).save("F:\\studentInfo.parquet");

        //开启Parquet数据源的自动合并元数据的特性
        //重点：sqlContext.read.option("mergeSchema", "true")
        DataFrame studentInfo = sqlContext.read().option("mergeSchema", "true")
                .parquet("F:\\studentInfo.parquet");

        //打印元数据结构信息
        studentInfo.printSchema();

        studentInfo.show();
    }
}
