package com.etc;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SaveMode;

/**
 * @author: fengze
 * @description:
 */
public class SaveModeTest {
    public static void main(String[] args) {
        SparkConf conf = new SparkConf()
                .setAppName("SaveModeTest")
                .setMaster("local");
        SparkContext sc= new SparkContext(conf);
        SQLContext sqlContext = new SQLContext(sc);

        DataFrame json = sqlContext.read().format("json").load("hdfs://weekend06:9000/emp/people.json");
        json.select("name")
        .save("hdfs://weekend06:9000/emp/peopleName_javaTest","json",SaveMode.Append);
    }
}
