package com.etc

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}


object ManuallySpecifyOptionsScala {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
      .setAppName("ManuallySpecifyOptionsScala")
      .setMaster("local")
    val sc = new SparkContext(conf)
    val sqlcontext = new SQLContext(sc)

    val frame = sqlcontext.read.format("json").load("hdfs://weekend06:9000/emp/people.json")

    frame.select("name").write.format("parquet").save("hdfs://weekend06:9000/emp/peopleName_scala")
  }
}
