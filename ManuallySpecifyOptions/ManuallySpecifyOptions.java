package com.etc;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;

/**
 * @author: fengze
 * @description:
 */
public class ManuallySpecifyOptions {
    public static void main(String[] args) {
        SparkConf conf = new SparkConf()
                .setAppName("ManuallySpecifyOptions")
                .setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(sc);
        DataFrame json = sqlContext.read().format("json")
                .load("hdfs://weekend06:9000/emp/people.json");

        json.select("name").write().format("parquet")
                .save("hdfs://weekend06:9000/emp/peopleName_java");
    }
}
