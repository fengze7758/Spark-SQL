package com.etc

import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.sql.types.DataTypes
import org.apache.spark.{SparkConf, SparkContext}

/**
  * author: fengze
  * description:SparkSQL销售额统计案例实战
  */
object DailySaleScala {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
      .setAppName("DailySaleScala")
      .setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)

    // 说明一下，业务的特点
    // 实际上呢，我们可以做一个，单独统计网站登录用户的销售额的统计
    // 有些时候，会出现日志的上报的错误和异常，比如日志里丢了用户的信息，那么这种，我们就一律不统计了
    // 模拟数据
    val data = Array("2015-10-01,55.05,1122",
      "2015-10-01,23.15,1133",
      "2015-10-01,15.20",
      "2015-10-02,56.05,1144",
      "2015-10-02,78.87,1155")

    // 并行化集合创建RDD，要通过并行化集合的方式创建RDD，那么就调用SparkContext以及其子类，的parallelize()方法
    val saleLogRDD = sc.parallelize(data, 5)

    // 进行有效销售日志的过滤
    val stringToBoolean: String => Boolean = log => (log.split(",").length >= 3)
    val filter = saleLogRDD.filter {
      stringToBoolean
    }

    // 将清洗后的数据 map 转换为RDD
    val mapRDD = filter.map { fm => (fm.split(",")(0), fm.split(",")(1).toDouble) }

    // 将RDD数据 map 转换为RDD<Row>形式，为后续转换为DataFrame作准备,
    // 把参数强制转换为Row类型然后返回，重点！！！

    val rowRDD = mapRDD.map(log => Row(log._1, log._2))

    // 创建元数据类型，为后续转换为DataFrame作准备
    val structFields = Array(DataTypes.createStructField("date", DataTypes.StringType, true),
      DataTypes.createStructField("sale_mount", DataTypes.DoubleType, true)
    )
    val structType = DataTypes.createStructType(structFields)

    // 转换为DataFrame
    val dataFrame = sqlContext.createDataFrame(rowRDD, structType)

    // DataFrame执行相关转换计算，开始进行每日销售额的统计；并转换为RDD
    val frameRDD = dataFrame.groupBy("date").sum("sale_mount").rdd.map { log => (log.getString(0), log.getDouble(1)) }

    //遍历RDD
    frameRDD.foreach { saleLog => println(saleLog._1 + " ,total sale : " + saleLog._2) }
  }
}
