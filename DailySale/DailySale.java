package com.etc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import akka.dispatch.sysmsg.Create;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.Tuple2;


/**
 * @author: fengze
 * @description: SparkSQL销售额统计案例实战
 */
public class DailySale {
    public static void main(String[] args) {
        SparkConf conf = new SparkConf()
                .setAppName("DailySale")
                .setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(sc);

        // 说明一下，业务的特点
        // 实际上呢，我们可以做一个，单独统计网站登录用户的销售额的统计
        // 有些时候，会出现日志的上报的错误和异常，比如日志里丢了用户的信息，那么这种，我们就一律不统计了
        // 模拟数据

        List<String> data = new ArrayList<String>();
        data.add("2015-10-01,55.05,1122");
        data.add("2015-10-01,23.15,1133");
        data.add("2015-10-01,15.20,");
        data.add("2015-10-02,56.05,1144");
        data.add("2015-10-02,78.87,1155");
        data.add("2015-10-02,113.02,1123");

        // 并行化集合创建RDD，要通过并行化集合的方式创建RDD，那么就调用SparkContext以及其子类，的parallelize()方法
        JavaRDD<String> saleLogRDD = sc.parallelize(data, 5);

        // 进行有效销售日志的过滤
        JavaRDD<String> filter = saleLogRDD.filter(new Function<String, Boolean>() {
            @Override
            public Boolean call(String line) throws Exception {
                boolean real = line.split(",").length >= 3;
                return real;
            }
        });

        // 将清洗后的数据 mapToPair 转换为RDD
        JavaPairRDD<String, Double> stringDoubleJavaPairRDD = filter.mapToPair(new PairFunction<String, String, Double>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Tuple2<String, Double> call(String s) throws Exception {
                return new Tuple2<String, Double>(s.split(",")[0], Double.valueOf(s.split(",")[1]));
            }
        });

        // 将RDD数据 map 转换为JavaRDD<Row>形式，为后续转换为DataFrame作准备
        JavaRDD<Row> map = stringDoubleJavaPairRDD.map(new Function<Tuple2<String, Double>, Row>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Row call(Tuple2<String, Double> tuple2) throws Exception {
                return RowFactory.create(tuple2._1, tuple2._2);
            }
        });

        // 创建元数据类型，为后续转换为DataFrame作准备
        List<StructField> structFields = Arrays.asList(DataTypes.createStructField("date", DataTypes.StringType, true),
                DataTypes.createStructField("sale_mount", DataTypes.DoubleType, true));
        StructType structType = DataTypes.createStructType(structFields);

        // 转换为DataFrame
        DataFrame dataFrame = sqlContext.createDataFrame(map, structType);

        // DataFrame执行相关转换计算，开始进行每日销售额的统计；并转换为JavaRDD
        JavaRDD<Tuple2<String, Double>> map1 = dataFrame.groupBy("date").sum("sale_mount")
                .javaRDD().map(new Function<Row, Tuple2<String, Double>>() {

            private static final long serialVersionUID = 1L;


            @Override
            public Tuple2<String, Double> call(Row row) throws Exception {
                return new Tuple2<String, Double>(row.getString(0), row.getDouble(1));
            }
        });

        // 遍历JavaRDD
        map1.foreach(new VoidFunction<Tuple2<String, Double>>() {

            private static final long serialVersionUID = 1L;

            @Override
            public void call(Tuple2<String, Double> tuple2) throws Exception {
                System.out.println(tuple2._1 + " ,total sale : " + tuple2._2);
            }
        });
        sc.close();
    }
}
