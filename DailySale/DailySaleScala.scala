package com.etc

import org.apache.spark.sql.types.DataTypes
import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * author: fengze
  * description:
  */
object DailySaleScala {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
      .setAppName("DailySaleScala")
      .setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)

    // 说明一下，业务的特点
    // 实际上呢，我们可以做一个，单独统计网站登录用户的销售额的统计
    // 有些时候，会出现日志的上报的错误和异常，比如日志里丢了用户的信息，那么这种，我们就一律不统计了
    // 模拟数据
    val lines = Array("2015-10-01,55.05,1122",
      "2015-10-01,23.15,1133",
      "2015-10-01,15.20",
      "2015-10-02,56.05,1144",
      "2015-10-02,78.87,1155")
    //并行化创建RDD，调用SparkContext的parallelize()方法
    val saleLogRDD = sc.parallelize(lines, 5)

    // 进行有效销售日志的过滤
    val filterRDD = saleLogRDD.filter { log => if (log.split(",").length == 3) true else false }

    // 将清洗过的RDD数据转换为JavaRDD<Row>形式，为后续转换为DataFrame作准备
    val map = filterRDD.map(log => Row(log.split(",")(0), log.split(",")(1).toDouble))

    // 创建元数据类型，为后续转换为DataFrame作准备
    val structFields = Array(DataTypes.createStructField("date", DataTypes.StringType, true),
      DataTypes.createStructField("sale_mount", DataTypes.DoubleType, true))

    val structType = DataTypes.createStructType(structFields)

    // 转换为DataFrame
    val dateframe = sqlContext.createDataFrame(map, structType)

    // DataFrame执行相关转换计算，开始进行每日销售额的统计；并转换为JavaRDD
    // 遍历rdd 并 println()输出
    dateframe.groupBy("date").sum("sale_mount")
      .rdd.map(row => (row.getString(0), " total sale : " + row.getDouble(1)))
      .collect().foreach(println(_))
  }
}
