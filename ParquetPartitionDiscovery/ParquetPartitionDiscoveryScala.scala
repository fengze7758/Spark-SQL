package com.etc

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}

/**
  * author: fengze
  * description:
  * Parquet数据源之自动推断分区
  * 有时我们可能并不想spark sql对数据进行自动分区，
  * 那么我们可以通过spark.sql.sources.partitionColumnTypeInference.enabled进行设置
  * 默认情况下它的值为true;
  */
object ParquetPartitionDiscoveryScala {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
      .setAppName("ParquetPartitionDiscoveryScala")
      .setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)

    val studentJson = sqlContext.read.json("F:\\studentInfo1.json")

    //打印元数据的结构信息
    studentJson.printSchema()

    studentJson.show()
  }
}
