package com.etc;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;

/**
 * @author: fengze
 * @description:
 * Parquet数据源之自动推断分区
 * 有时我们可能并不想spark sql对数据进行自动分区，
 * 那么我们可以通过spark.sql.sources.partitionColumnTypeInference.enabled进行设置
 * 默认情况下它的值为true;
 */
public class ParquetPartitionDiscovery {
    public static void main(String[] args) {
        SparkConf conf = new SparkConf()
                .setAppName("ParquetPartitionDiscovery")
                .setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(sc);

        DataFrame json = sqlContext.read().json("F:\\studentInfo1.json");

        //打印元数据的结构信息
        json.printSchema();

        json.show();
        //root
        // |-- age: long (nullable = true)
        // |-- name: string (nullable = true)
    }
}
