package com.etc;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author: fengze
 * @description: 每日UV，用户进行去重以后的访问总数
 */
public class DailyUV {
    public static void main(String[] args) {
        SparkConf conf = new SparkConf()
                .setAppName("DailyUV")
                .setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(sc);

        // 构造用户访问日志数据，并创建DataFrame

        // 模拟用户访问日志，日志用逗号隔开，第一列是日期，第二列是用户id
        List<String> list = Arrays.asList(
                "2015-10-01,1122",
                "2015-10-01,1122",
                "2015-10-01,1123",
                "2015-10-01,1124",
                "2015-10-01,1124",
                "2015-10-01,1128",
                "2015-10-01,1129",
                "2015-10-02,1122",
                "2015-10-02,1121",
                "2015-10-02,1123",
                "2015-10-02,1125",
                "2015-10-02,1123");
        JavaRDD<String> UVRDD = sc.parallelize(list);

        // 将模拟出来的用户访问日志RDD，转换为DataFrame

        // 首先，将普通的RDD，转换为元素为Row的RDD
        JavaRDD<Tuple2<String, Long>> map = UVRDD.map(new Function<String, Tuple2<String, Long>>() {
            @Override
            public Tuple2<String, Long> call(String s) throws Exception {
                return new Tuple2<String, Long>(s.split(",")[0], Long.valueOf(s.split(",")[1]));
            }
        });
        JavaRDD<Row> map1 = map.map(new Function<Tuple2<String, Long>, Row>() {
            @Override
            public Row call(Tuple2<String, Long> tuple2) throws Exception {
                return RowFactory.create(tuple2._1, tuple2._2);
            }
        });

        // 构造DataFrame的元数据
        List<StructField> structFields = new ArrayList<StructField>();
        structFields.add(DataTypes.createStructField("date",DataTypes.StringType,true));
        structFields.add(DataTypes.createStructField("userID",DataTypes.LongType,true));
        StructType structType = DataTypes.createStructType(structFields);

        DataFrame df = sqlContext.createDataFrame(map1, structType);


        // 这里讲解一下uv的基本含义和业务
        // 每天都有很多用户来访问，但是每个用户可能每天都会访问很多次
        // 所以，uv，指的是，对用户进行去重以后的访问总数
        // 将DataFrame注册为临时表，便于后面查询数据
        df.registerTempTable("accessInfo");

        // 查询每一天，不同用户登录次数
        DataFrame everyDayUVDF = sqlContext.sql(
                "SELECT date, COUNT(DISTINCT(userID)) AS count FROM accessInfo GROUP BY date");

        // 将DataFrame转换为JavaRDD，为后续遍历数据做准备
        JavaRDD<Tuple2<String, Long>> userAccessLogSumRDD =
                everyDayUVDF.javaRDD().map(new Function<Row, Tuple2<String, Long>>() {
                    private static final long serialVersionUID = 1L;
                    @Override
                    public Tuple2<String, Long> call(Row row) throws Exception {
                        return new Tuple2<String, Long>(row.getString(0), row.getLong(1));
                    }
                });

        // 遍历计算后的结果数据
        userAccessLogSumRDD.foreach(new VoidFunction<Tuple2<String,Long>>() {
            private static final long serialVersionUID = 1L;
            @Override
            public void call(Tuple2<String, Long> tt) throws Exception {
                System.out.println(tt._1.toString() + " login: " + tt._2.toString() + " times");
            }
        });


        sc.close();
    }
}
