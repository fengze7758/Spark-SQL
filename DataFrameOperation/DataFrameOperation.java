package com.etc;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;

/**
 * @author: fengze
 * @description:
 * DataFrame的常用操作
 */
public class DataFrameOperation {
    public static void main(String[] args) {
        //创建DataFrame
        SparkConf conf = new SparkConf()
                .setAppName("DataFrameOperation");
        JavaSparkContext sc = new JavaSparkContext(conf);

        SQLContext sqlContext = new SQLContext(sc);

        DataFrame json = sqlContext.read().json("hdfs://weekend06:9000/emp/students.json");

        //打印DataFrame中所有的数据（select * from ...）
        json.show();

        //打印DataFrame的元数据（Schema）
        json.printSchema();

        //查询某列所有的数据
        json.select("name").show();

        //查询某几列所有的数据，并对列进行计算
        json.select(json.col("name"),json.col("age").plus(1)).show();

        //根据某一列的值进行过滤
        json.filter(json.col("age").gt(18)).show();

        //根据某一列进行分组，然后进行聚合
        json.groupBy(json.col("age")).count().show();


    }
}