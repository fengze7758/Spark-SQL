package com.etc

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}


object DataFrameOperationScala {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("DataFrameOperationScala")

    val sc = new SparkContext(conf)

    val sqlContext = new SQLContext(sc)

    val dataFrame = sqlContext.read.json("hdfs://weekend06:9000/emp/students.json")

    //打印DataFrame中所有的数据（select * from ...）
    dataFrame.show()

    //打印DataFrame的元数据（Schema）
    dataFrame.schema

    //查询某列所有的数据
    dataFrame.select("name").show()

    //查询某几列所有的数据，并对列进行计算
    dataFrame.select(dataFrame.col("name"),dataFrame.col("age").plus(1)).show()

    //根据某一列的值进行过滤
    dataFrame.filter(dataFrame.col("age").gt(18)).show()

    //根据某一列进行分组，然后进行聚合
    dataFrame.groupBy(dataFrame.col("age")).count().show()
  }
}
