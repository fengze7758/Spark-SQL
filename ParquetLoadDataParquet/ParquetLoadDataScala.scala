package com.etc

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Parquet数据源之使用编程方式加载数据
  */
object ParquetLoadDataScala {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
      .setAppName("ParquetLoadDataScala")
      .setMaster("local")
    val sc = new SparkContext(conf)
    val sqlcontext = new SQLContext(sc)

    // 读取Parquet文件中的数据，创建一个DataFrame
    val frameDf = sqlcontext.read.parquet("D:\\Documents\\Tencent Files\\1433214538\\FileRecv\\第一阶段代码\\第76讲-Spark SQL：数据源之通用的load和save操作\\文档\\users.parquet")

    //将DataFrame注册为临时表，然后使用SQL查询需要的数据
    frameDf.registerTempTable("user")
    val userDf = sqlcontext.sql("select * from user")

    // 对查询出来的DataFrame进行transformation操作，处理数据，然后打印出来
    userDf.rdd.map { row => "Name:" + row.get(0) }.collect()
      .foreach(username => println(username))
  }
}
