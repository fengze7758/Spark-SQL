package com.etc;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;

import java.util.List;

/**
 * @author: fengze
 * @description:
 * Parquet数据源之使用编程方式加载数据
 */
public class ParquetLoadData {
    public static void main(String[] args) {
        SparkConf conf = new SparkConf()
                .setAppName("ParquetLoadData")
                .setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(sc);

        // 读取Parquet文件中的数据，创建一个DataFrame
        DataFrame userDf = sqlContext.read().parquet("D:\\Documents\\Tencent Files\\1433214538\\FileRecv\\第一阶段代码\\第76讲-Spark SQL：数据源之通用的load和save操作\\文档\\users.parquet");

        //将DataFrame注册为临时表，然后使用SQL查询需要的数据
        userDf.registerTempTable("user");
        DataFrame sql = sqlContext.sql("select * from user");

        // 对查询出来的DataFrame进行transformation操作，处理数据，然后打印出来
        JavaRDD<String> map1 = sql.javaRDD().map(new Function<Row, String>() {
            @Override
            public String call(Row row) throws Exception {
                return "Name:" + row.getString(0);
            }
        });
        List<String> collect = map1.collect();
        for (String username : collect) {
            System.out.println(username);
        }
    }
}
