package com.etc;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.Arrays;
import java.util.List;

/**
 * @author: fengze
 * @description: SparkSQL编写UDF自定义函数（JAVA）
 */
public class UDF {
    public static void main(String[] args) {
        SparkConf conf = new SparkConf()
                .setAppName("UDF")
                .setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(sc);

        //构造模拟数据
        List<String> names = Arrays.asList("Leo", "Marry", "Jack", "Tom");
        JavaRDD<String> namesRDD = sc.parallelize(names, 5);
        JavaRDD<Row> map1 = namesRDD.map(new Function<String, Row>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Row call(String s) throws Exception {
                return RowFactory.create(s);
            }
        });

        // 构造元数据
        List<StructField> structFields = Arrays.asList
                (DataTypes.createStructField("name", DataTypes.StringType, true));
        StructType structType = DataTypes.createStructType(structFields);

        // 创建DataFrame
        DataFrame namesDF = sqlContext.createDataFrame(map1, structType);

        // 注册一张names表
        namesDF.registerTempTable("names");

        // 定义和注册自定义函数
        // 定义函数：自己写匿名函数
        // 注册函数：SQLContext.udf.register()
        /**
         * Function可以使用UDF1到UDF22/21，所表达的意思就是几个参数，2代就指两个参数，10代指10个参数
         * return 返回的即为UDF<> 的最后一个参数
         */
        sqlContext.udf().register("strLen", new UDF1<String, Integer>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Integer call(String s) throws Exception {
                return s.length();
            }
        }, DataTypes.IntegerType);


        // 使用自定义函数，查询数据
        List<Row> nameData = sqlContext.sql("select name,strLen(name) from names").javaRDD().collect();

        for (Row row : nameData) {
            System.out.println("name:" + row.get(0) + "  长度：" + row.get(1));
        }

        sc.close();
    }
}

        //name:Leo  长度：3
        //name:Marry  长度：5
        //name:Jack  长度：4
        //name:Tom  长度：3
