package com.etc;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;

/**
 * @author: fengze
 * @description:
 * 通用的load和save操作
 */
public class GenericLoadSave {
    public static void main(String[] args) {
        SparkConf conf = new SparkConf()
                .setAppName("GenericLoadSave")
                .setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(sc);
        //读取 加载 =》 写 保存
        DataFrame load = sqlContext.read().load("hdfs://weekend06:9000/emp/users.parquet");
        load.select("name","favorite_color").write()
                .save("hdfs://weekend06:9000/emp/namesAndFavColors.parquet");
    }
}
