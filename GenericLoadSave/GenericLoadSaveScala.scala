package com.etc

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}


object GenericLoadSaveScala {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
      .setAppName("GenericLoadSaveScala")
      .setMaster("local")
    val sc = new SparkContext(conf)
    val sqlcontext = new SQLContext(sc)

    val frame = sqlcontext.read.load("hdfs://weekend06:9000/emp/users.parquet")

    frame.write.save("hdfs://weekend06:9000/emp/namesAndFavColors_scala.parquet")

  }
}
